# Security Policy

## Supported Versions

Well this project can be supported on any device with a browser to view it and it can be inserted in any
ATM machine.

| Version | Supported          |
| ------- | ------------------ |
| 1.0   | :white_check_mark: |

## Reporting a Vulnerability

I am a 14 year old developer and yes I can make mistake. Please create an issue and wait till I or someone developer create a file for you.
